package com.day5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class exercise5 {
    public static void main(String[] args) throws InterruptedException {
        String arr[] = { "apple", "selenium", "Cucumber" };
        String wid[] = new String[3];
        String title[] = new String[3];

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://www.google.com/?q=" + arr[0]);
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//*[@id=\"APjFqb\"]")).submit();
        wid[0] = driver.getWindowHandle();
        title[0] = driver.getTitle();
        Thread.sleep(1000);
        driver.close();

        WebDriverManager.edgedriver().setup();
        WebDriver driver2 = new EdgeDriver();
        driver2.navigate().to("https://www.google.com/?q=" + arr[1]);
        driver2.manage().window().maximize();
        driver2.findElement(By.xpath("//*[@id=\"APjFqb\"]")).submit();
        wid[1] = driver2.getWindowHandle();
        title[1] = driver2.getTitle();
        Thread.sleep(1000);
        driver2.close();

        WebDriverManager.edgedriver().setup();
        WebDriver driver3 = new EdgeDriver();
        driver3.navigate().to("https://www.google.com/?q=" + arr[2]);
        driver3.manage().window().maximize();
        driver3.findElement(By.xpath("//*[@id=\"APjFqb\"]")).submit();
        wid[2] = driver3.getWindowHandle();
        title[2] = driver3.getTitle();
        Thread.sleep(1000);
        driver3.close();

        for (int i = 0; i < 3; i++) {
            System.out.println("Query     : " + arr[i]);
            System.out.println("Window ID : " + wid[i]);
            System.out.println("Title     : " + title[i]);
            System.out.println();
        }
    }

}